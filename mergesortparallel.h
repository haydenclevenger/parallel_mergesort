/*
This header file declares globals for parallel and serial mergesort

Hayden Clevenger
haydenclevenger@sandiego.edu
October 13, 2016
*/

// forward declarations
int bad_args(int argc, char *argv[]);
int identical();
int sorted();
double serial();
double parallel();
void serial_merge_sort(int start, int end, int *arr);
void *mergeSortParallel(void *rank);
void split(int start, int end, int *arr);
void merge(int start, int middle, int end, int *arr);
void copy_array(int start, int end, int *arr);
void print_statistics(double serial_msec, double parallel_msec);
void sync_init();
