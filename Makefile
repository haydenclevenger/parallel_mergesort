CC = gcc
CFLAGS = -Wall -Werror -Wextra

all: mergesort

mergesort: mergesort.o mergesortparallel.o
	$(CC) $(CFLAGS) -o mergesort mergesort.o mergesortparallel.o

mergesort.o: mergesort.c
	$(CC) $(CFLAGS) -c mergesort.c

mergesortparallel.o: mergesortparallel.c
	$(CC) $(CFLAGS) -c mergesortparallel.c

clean:
	$(RM) mergesort *.o
