/*
This file defines a parallel implementation of the mergesort algorithm.

Hayden Clevenger
haydenclevenger@sandiego.edu
October 13, 2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

//globals
extern int *vecParallel;
extern int *vecSerial;
extern int *temp;
extern long n;	//size of vectors
extern long threadCount;
extern pthread_mutex_t *lock;
extern pthread_cond_t *can_proceed;
int counter = 0;

// forward declarations
extern int bad_args(int argc, char *argv[]);
extern int identical();
extern int sorted();
extern void serial_merge_sort(int start, int end, int *arr);
extern void *mergeSortParallel(void *rank);
extern void split(int start, int end, int *arr);
extern void merge(int start, int middle, int end, int *arr);
extern void copy_array(int start, int end, int *arr);
extern void print_statistics(double serial_msec, double parallel_msec);
void section(long rank, long *start, long *last);
void parallel_merge(long rank, long step, long start_x, long end_x, long start_y, long end_y, long f_thread, long l_thread, long dest);
void nc_merge(long start_x, long end_x, long start_y, long end_y, long dest);
void synchronize();
long binary_search(int value, long start, long end);

/*
Parallel mergesort
@param thread_num Individual thread number
*/
void *mergeSortParallel(void *thread_num) {

	long rank = (long) thread_num;

	//determine thread start and end point
	long first, last;
	section(rank, &first, &last);

	//sort subarray using serial mergesort
	serial_merge_sort(first, last, vecParallel);

	//make sure threads have completed subarray sorting
	synchronize();

	//merge sorted subarrays
	long step = 2, count = 1;
	while(count < threadCount) {
		long f_thread, l_thread;
		f_thread = rank - (rank % step);
		l_thread = f_thread + step < threadCount ? f_thread + step - 1 : threadCount - 1;
		//checking if partner exists (accounts for array size not being a power of two)
		if(f_thread + count < threadCount) {
			long start_x, end_x, start_y, end_y, temp;
			section(f_thread, &start_x, &temp); //start_x
			section(f_thread + count - 1, &temp, &end_x); //end_x
			start_y = end_x; //start_y
			section(l_thread, &temp, &end_y); //end_y

			//call to parallel merge (always copy to start of left sub array)
			parallel_merge(rank, step, start_x, end_x, start_y, end_y, f_thread, l_thread, start_x);
		}

		//copy from temp back into vecParallel
		synchronize();
		copy_array(first, last, vecParallel);
		synchronize();

		step *= 2;
		count *= 2;
	}

	return NULL;	//to appease gcc gods
}

/*
Parallel implementation of merging of sorted subarrays
@param rank This particular threads ID
@param step Determines which thread is merging
@param start_x Beginning of first half
@param end_x End of first half
@param start_y Beginning of second half
@param end_y End of second half
@param f_thread First thread rank in group
@param l_thread Last thread rank in group
@param dest Index to copy into
*/
void parallel_merge(long rank, long step, long start_x, long end_x, long start_y, long end_y, long f_thread, long l_thread, long dest) {

	//base case
	if(l_thread == f_thread) { 
		//merge region
		return nc_merge(start_x, end_x, start_y, end_y, dest);
	}

	//recursive call
	else {
		//calculate X1, X2, Y1, and Y2
		long x_div, y_div;
		x_div = start_x + ((end_x - start_x) / 2);
		y_div = binary_search(vecParallel[x_div], start_y, end_y);

		//decide whether to go right or left
		long middle = f_thread + ((l_thread - f_thread) / 2);

		//left (merge X1 and Y1)
		if(rank <= middle) {
			return parallel_merge(rank, step, start_x, x_div, start_y, y_div, f_thread, middle, dest);
		}
		//right (merge X2 and Y2)
		else {
			dest += (x_div - start_x) + (y_div - start_y);
			return parallel_merge(rank, step, x_div, end_x, y_div, end_y, middle + 1, l_thread, dest);
		}
	}
}

/*
Binary search to help determine Y1 and Y2 divisor
@param value Lowest element in X2
@param start Start of Y
@param end End of Y
@return Lowest index of vecParallel whose value is greater than value
*/
long binary_search(int value, long start, long end) {
	//base case
	if(end - start <= 1) {
		return vecParallel[start] > value ? start : end;
	}
	long mid = start + ((end - start) / 2);
	//search upper half
	if(vecParallel[mid] < value) {
		return binary_search(value, mid, end);
	}
	//search lower half
	else {
		return binary_search(value, start, mid);
	}
}

/*
Function to implement merging of noncontiguous subarrays
@param start_x Beginning index of first subarray to merge
@param end_x End index of first subarray to merge
@param start_y Beginning index of second subarray to merge
@param end_y End index of second subarray to merge
@param dest Destination index to copy into
*/
void nc_merge(long start_x, long end_x, long start_y, long end_y, long dest) {
	long i = start_x;
	long j = start_y;
	long length = (end_x - start_x) + (end_y - start_y);
	//while elements still in left or right sections
	for(int k = dest; k < length + dest; k++) {
		//if left subarray still has elements and value is < right subarray
		if(i < end_x && (j >= end_y || vecParallel[i] <= vecParallel[j])) {
			temp[k] = vecParallel[i];
			i++;
		}
		else {
			temp[k] = vecParallel[j];
			j++;
		}
	}
}

/*
Function to determine thread's section of array
Return values are saved in params first and last
@param rank Thread's rank
@param first Index of start of threads array section
@param last Index one beyond end of threads array section
@return first and last through pointers
*/
void section(long rank, long *first, long *last) {
	if(rank >= threadCount) {
		printf("Error in section function: rank (%ld) too big\n", rank);
		exit(1);
	}
	long quotient = n / threadCount;
	long remainder = n % threadCount;
	long count;
	if(rank < remainder) { //assign one extra value
		count = quotient + 1;
		*first = rank * count;
	}
	else {
		count = quotient;
		*first = (rank * count) + remainder;
	}
	*last = *first + count;
}

/*
Barrier synchronization
*/
void synchronize() {
	pthread_mutex_lock(lock);

	counter++;

	//release threads
	if(counter == threadCount) {
		counter = 0;
		pthread_cond_broadcast(can_proceed);
	}
	//wait on condition variable
	else {
		while(pthread_cond_wait(can_proceed, lock));
	}

	pthread_mutex_unlock(lock);
}

/*
Serial Mergesort implementation for O(nlogn) sorting
Sorts the global array arr
@param start Is start of where to sort arary
@param end Is end of where to sort array
@param arr Is the array to sort
*/
void serial_merge_sort(int start, int end, int *arr) {
	split(start, end, arr);
}

/*
Split algorithm for implementation in mergesort
@param start Is the start index to sort from (inclusive)
@param end Is the last index to sort to (exclusive)
@param arr Is the array to sort
*/
void split(int start, int end, int *arr) {
	if (end - start < 2) return;

	int middle = (start + end) / 2;
	split(start, middle, arr);
	split(middle, end, arr);
	merge(start, middle, end, arr);
	copy_array(start, end, arr);
}

/*
Merge algorithm for implementation in mergesort
@param start Is the start index to sort from
@param middle Is the middle index seperating the halves
@param end Is the last index to sort to
@param arr Is the array to sort
*/
void merge(int start, int middle, int end, int *arr) {
	int i = start;
	int j = middle;
	//while elements still in left or right sections
	for(int k = start; k < end; k++) {
		//if left half still has elements and value is < right half
		if(i < middle && (j >= end || arr[i] <= arr[j])) {
			temp[k] = arr[i];
			i++;
		}
		else {
			temp[k] = arr[j];
			j++;
		}
	}
}

/*
Copy function for use in mergesort
Copies from temp to arr
@param start Is start index for copying
@param end Is end index for copying
@param arr Is the array to copy into
*/
void copy_array(int start, int end, int *arr) {
	for(int i = start; i < end; i++)
		arr[i] = temp[i];
}
