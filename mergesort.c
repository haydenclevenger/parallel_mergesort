/*
This program is designed to compare serial and parallel mergesort
Takes in number of threads and array size as parameters
Prints out several values
	Number of threads
	Array size
	Time to sort with serial mergesort
	Time to sort with parallel mergesort
	Speedup
	Efficiency

It also prints an error message if either of the 
two arrays are not correctly sorted.

Hayden Clevenger
haydenclevenger@sandiego.edu
October 4, 2016
*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

#include "mergesortparallel.h"

//globals
int *vecParallel;
int *vecSerial;
int *temp;
long n;	//size of array
long threadCount;
pthread_mutex_t *lock;
pthread_cond_t *can_proceed;

int main(int argc, char **argv) {
	//check arguments
	if(bad_args(argc, argv)) {
		exit(1);
	}

	//array setup
	vecParallel = malloc(sizeof(int) * n);
	vecSerial = malloc(sizeof(int) * n);
	temp = malloc(sizeof(int) * n);
	if(vecSerial == NULL || vecParallel == NULL || temp == NULL) {
		printf("Error mallocing arrays\n");
		exit(1);
	}
	//fill arrays with identical random values
	srand(0);
	for(int i = 0; i < n; i++) {
		vecParallel[i] = vecSerial[i] = rand();
	}

	//serial merge sort
	double serial_sec = serial();

	//parallel merge sort
	double parallel_sec = parallel();

	//print statistics
	print_statistics(serial_sec, parallel_sec);

	//free 
	free(vecParallel);
	free(vecSerial);
	free(temp);
}

/*
Function wrapper for parallel merge sorting. Allows for timing.
@return parallel algorithm run time in seconds
*/
double parallel() {
	struct timeval start_time, end_time;
	//start parallel timer
	gettimeofday(&start_time, NULL);
	//initialize mutex and condition variables for synchronization
	sync_init();
	//declare threads and launch them to sort parallel array
	pthread_t *thread_handles = malloc(threadCount * sizeof(pthread_t));
	for(long i = 1; i < threadCount; i++) {
		pthread_create(&thread_handles[i], NULL, mergeSortParallel, (void*)i);
	}
	//launch main thread
	mergeSortParallel(0);
	//join up with all threads
	for(int i = 1; i < threadCount; i++) {
		pthread_join(thread_handles[i], NULL);
	}
	free(thread_handles);
	//end serial timer
	gettimeofday(&end_time, NULL);
	//convert to msec
	double parallel_sec = (double)(end_time.tv_sec - start_time.tv_sec) + (double)(end_time.tv_usec - start_time.tv_usec) / 1000000;
	//validate
	if(!(identical())) { printf("Parallel array not sorted!\n"); }

	return parallel_sec;
}

/*
Function wrapper for serial merge sorting. Allows for timing.
@return serial algorithm run time in seconds
*/
double serial() {
	struct timeval start_time, end_time;
	//start serial timer
	gettimeofday(&start_time, NULL);
	//sort serial array
	serial_merge_sort(0, n, vecSerial);
	//end serial timer
	gettimeofday(&end_time, NULL);
	//convert to msec
	double serial_sec = (double)(end_time.tv_sec - start_time.tv_sec) + (double)(end_time.tv_usec - start_time.tv_usec) / 1000000;
	//validate
	if(!(sorted(vecSerial))) { printf("Serial array not sorted!\n"); }
	return serial_sec;
}

/*
Function to initialize synchronization tools for threads
Tools include mutex and condition variables to make a barrier
*/
void sync_init() {
	if((lock = malloc(sizeof(pthread_mutex_t))) == NULL) {
		printf("Error allocating memory for lock\n");
		exit(1);
	}
	if((can_proceed = malloc(sizeof(pthread_cond_t))) == NULL) {
		printf("Error allocating memory for condition variable\n");
		exit(1);
	}
	int error; //for error codes
	if((error = pthread_mutex_init(lock, NULL)) != 0) {
		printf("Error initializing mutex\n");
		exit(error);
	}
	if((error = pthread_cond_init(can_proceed, NULL)) != 0) {
		printf("Error initializing condition variable\n");
		exit(error);
	}
}

/*
Function to print statistics gathered from mergesort functions
@param serial_msec is the time to sort serially in milliseconds
@param parallel_msec is the time to sort in parallel in milliseconds
*/
void print_statistics(double serial_msec, double parallel_msec) {
	printf("Number of threads: %ld\n", threadCount);
	printf("Size: %ld\n", n);
	printf("Serial time: %f milliseconds\n", serial_msec);
	printf("Parallel time: %f milliseconds\n", parallel_msec);
	printf("Speedup: %f\n", serial_msec / parallel_msec);
	printf("Efficiency: %f\n", (serial_msec / parallel_msec) / threadCount);
}

/*
Function to see if vecSerial and vecParallel are identical
@return 1 if matching, 0 otherwise
*/
int identical() {
	for(int i = 0; i < n; i++) {
		if(vecSerial[i] != vecParallel[i]) {
			return 0;
		}
	}
	return 1;
}

/*
Function that validates that vecSerial is sorted
@param arr Array to check
@return 1 if correctly sorted, otherwise 0
*/
int sorted(int *arr) {
	for(int i = 1; i < n; i++) {
		if(arr[i-1] > arr[i])
			return 0;
	}
	return 1;
}

/*
Function to check command line args
@param argc is number of command line args
@param argv[][] is command line args
@return 1 if everything ok, otherwise 0
*/
int bad_args(int argc, char *argv[]) {
	char *ptr;
	if(argc != 3) {
		printf("Usage: ./mergesort <number threads> <array size>\n");
		return 1;
	}
	threadCount = atoi(argv[1]);
	n = strtol(argv[2], &ptr, 10);
	if(n < 1 || threadCount < 1) {
		printf("Size and num_threads must be greater than one\n");
		return 1;
	}
	return 0;
}
